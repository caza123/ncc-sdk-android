package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccAccelerometerData;
import com.agero.nccsdk.domain.data.NccSensorData;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;

/**
 * Created by james hermida on 12/5/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class NccAccelerometerMapperTest {

    private AbstractSensorDataMapper mapper = new NccAccelerometerMapper();

    @Test
    public void map() throws Exception {
        long timeUtc = 1315314000000L;
        String formattedDate = mapper.getReadableTimestamp(timeUtc);
        float x = 1.9999f;
        float y = 9.1111f;
        float z = 1.9191f;
        NccSensorData data = new NccAccelerometerData(timeUtc, x, y, z);

        String expected = "RA," + x + "," + y + "," + z + ",NA,NA,NA,NA,NA,NA," + timeUtc + "," + formattedDate;
        String actual = mapper.map(data);

        assertEquals(expected, actual);
    }
}
