package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccMotionData;
import com.agero.nccsdk.domain.data.NccSensorData;
import com.google.android.gms.location.DetectedActivity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static com.agero.nccsdk.ubi.collection.mapper.NccMotionActivityMapper.ACTIVITY_DETECTED;
import static com.agero.nccsdk.ubi.collection.mapper.NccMotionActivityMapper.ACTIVITY_NOT_DETECTED;
import static junit.framework.Assert.assertEquals;

/**
 * Created by james hermida on 12/5/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class NccMotionActivityMapperTest {

    private AbstractSensorDataMapper mapper = new NccMotionActivityMapper();

    @Test
    public void map() throws Exception {
        long timeUtc = 1315314000000L;
        String formattedDate = mapper.getReadableTimestamp(timeUtc);
        int detectedActivity = DetectedActivity.IN_VEHICLE;
        int confidence = 50;
        NccSensorData data = new NccMotionData(timeUtc, detectedActivity, confidence);

        String expected = "Z," +
                ACTIVITY_NOT_DETECTED + "," +
                ACTIVITY_NOT_DETECTED + "," +
                ACTIVITY_NOT_DETECTED + "," +
                ACTIVITY_DETECTED + "," +
                ACTIVITY_NOT_DETECTED + "," +
                ACTIVITY_NOT_DETECTED + "," +
                String.valueOf(confidence) + "," +
                "NA,NA," +
                timeUtc + "," +
                formattedDate;
        String actual = mapper.map(data);

        assertEquals(expected, actual);
    }
}
