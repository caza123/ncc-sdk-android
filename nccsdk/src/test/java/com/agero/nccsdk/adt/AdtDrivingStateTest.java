package com.agero.nccsdk.adt;

import android.content.Context;

import com.agero.nccsdk.ubi.collection.SensorCollector;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Field;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(PowerMockRunner.class)
public class AdtDrivingStateTest {

    private AdtDrivingState adtDrivingState;

    @Mock
    private Context mockContext;

    @Mock
    private SensorCollector mockEndMonitor;

    @Mock
    private AdtStateMachine mockAdtStateMachine;

    @Before
    public void setUp() throws Exception {
        adtDrivingState = PowerMockito.spy(new AdtDrivingState(mockContext));
    }

    /* FIXME requires NccSdk.getComponent() called from AdtEndMonitor.start() to be mocked
    @Test
    @PrepareForTest({AdtDrivingState.class})
    public void onEnter() throws Exception {
        mockPrivateField(adtDrivingState, "mEndMonitor", mockEndMonitor);
        PowerMockito.doNothing().when(adtDrivingState, "sendSessionStartBroadcast");
        adtDrivingState.onEnter();
        verify(mockEndMonitor).start();
    }
    */

    @Test
    @PrepareForTest({AdtDrivingState.class})
    public void onExit() throws Exception {
        mockPrivateField(adtDrivingState, "endMonitor", mockEndMonitor);
        PowerMockito.doNothing().when(adtDrivingState, "sendSessionEndBroadcast");
        adtDrivingState.onExit();
        verify(mockEndMonitor).stop();
    }

    @Test
    public void startMonitoring() {
        adtDrivingState.startMonitoring(mockAdtStateMachine);
        verifyZeroInteractions(mockAdtStateMachine);
    }

    @Test
    public void stopMonitoring() {
        adtDrivingState.stopMonitoring(mockAdtStateMachine);
        verifyZeroInteractions(mockAdtStateMachine);
    }

    @Test
    public void startDriving() {
        adtDrivingState.startDriving(mockAdtStateMachine);
        verifyZeroInteractions(mockAdtStateMachine);
    }

    @Test
    public void stopDriving() {
        adtDrivingState.stopDriving(mockAdtStateMachine);
        verify(mockAdtStateMachine).setState(any(AdtMonitoringState.class));
    }


    private void mockPrivateField(Object targetObject, String fieldName, Object fieldValue) throws Exception {
        Field field = AdtDrivingState.class.getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(targetObject, fieldValue);
    }

}
