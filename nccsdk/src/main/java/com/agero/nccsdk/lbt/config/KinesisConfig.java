package com.agero.nccsdk.lbt.config;

import com.agero.nccsdk.internal.common.statemachine.SdkConfig;

/**
 * Created by james hermida on 11/8/17.
 */

public class KinesisConfig implements SdkConfig {

    private long uploadInterval = 1000 * 60; // default 1 minute

    public KinesisConfig() {
    }

    public KinesisConfig(long uploadInterval) {
        this.uploadInterval = uploadInterval;
    }

    public long getUploadInterval() {
        return uploadInterval;
    }

    public void setUploadInterval(long uploadInterval) {
        this.uploadInterval = uploadInterval;
    }
}
