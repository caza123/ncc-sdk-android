package com.agero.nccsdk.lbt;

import com.agero.nccsdk.NccSensorListener;
import com.agero.nccsdk.internal.common.statemachine.StateMachine;
import com.agero.nccsdk.internal.log.Timber;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract class LbtAbstractState implements LbtState {

    List<NccSensorListener> trackingListeners = new CopyOnWriteArrayList<>();

    @Override
    public boolean addTrackingListener(StateMachine<LbtState> machine, NccSensorListener sensorListener) {
        if (sensorListener != null) {
            if (!trackingListeners.contains(sensorListener)) {
                if (trackingListeners.add(sensorListener)) {
                    Timber.d("Registered %s for LBT data", getClassNameAndHashCode(sensorListener));
                    printListeners();
                    return true;
                } else {
                    Timber.w("Failed to add listener %s for LBT data", getClassNameAndHashCode(sensorListener));
                }
            } else {
                Timber.w("%s has already been registered for LBT data", getClassNameAndHashCode(sensorListener));
            }
        } else {
            Timber.w("Attempted to add a null tracking listener");
        }

        return false;
    }

    @Override
    public boolean removeTrackingListener(StateMachine<LbtState> machine, NccSensorListener sensorListener) {
        if (sensorListener != null) {
            if (trackingListeners.contains(sensorListener)) {
                if (trackingListeners.remove(sensorListener)) {
                    Timber.d("Unregistered %s for LBT data", getClassNameAndHashCode(sensorListener));
                    printListeners();
                    return true;
                } else {
                    Timber.w("Failed to remove listener %s for LBT data", getClassNameAndHashCode(sensorListener));
                }
            } else {
                Timber.w("%s has not been registered for LBT data", getClassNameAndHashCode(sensorListener));
            }
        } else {
            Timber.w("Attempted to remove a null tracking listener");
        }

        return false;
    }

    private String getClassNameAndHashCode(NccSensorListener sensorListener) {
        if (sensorListener != null) {
            return sensorListener.getClass().getName() + "@" + sensorListener.hashCode();
        }

        return "";
    }

    private void printListeners() {
        if (trackingListeners != null) {
            StringBuilder log = new StringBuilder("Current LBT listeners: ");
            for (int i = 0; i < trackingListeners.size(); i++) {
                log.append("(").append((i + 1)).append(") ").append(getClassNameAndHashCode(trackingListeners.get(i))).append(" ");
            }
            Timber.d(log.toString());
        }
    }
}
