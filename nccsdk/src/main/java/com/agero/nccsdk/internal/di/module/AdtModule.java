package com.agero.nccsdk.internal.di.module;

import android.content.Context;

import com.agero.nccsdk.adt.AdtInactiveState;
import com.agero.nccsdk.adt.AdtState;
import com.agero.nccsdk.adt.AdtStateMachine;
import com.agero.nccsdk.adt.detection.domain.geofence.GeofenceManager;
import com.agero.nccsdk.adt.event.AdtEventHandler;
import com.agero.nccsdk.domain.SensorManager;
import com.agero.nccsdk.internal.common.statemachine.StateMachine;
import com.agero.nccsdk.internal.data.preferences.NccSharedPrefs;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.LocationServices;

import java.util.concurrent.Executor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AdtModule {

    @Provides
    GeofencingClient provideGeofencingClient(Context context) {
        return LocationServices.getGeofencingClient(context);
    }

    @Provides
    @Singleton
    GeofenceManager provideGeofenceManager(Context context, NccSharedPrefs sharedPrefs, GeofencingClient geofencingClient, SensorManager sensorManager, Executor executor) {
        return new GeofenceManager(context, geofencingClient, sharedPrefs, sensorManager, executor);
    }

    @Provides
    @Singleton
    StateMachine<AdtState> provideAdtStateMachine(Context context) {
        return new AdtStateMachine(new AdtInactiveState(context));
    }

    @Provides
    @Singleton
    AdtEventHandler provideAdtEventHandler(StateMachine<AdtState> adtStateStateMachine) {
        return new AdtEventHandler(adtStateStateMachine);
    }
}
