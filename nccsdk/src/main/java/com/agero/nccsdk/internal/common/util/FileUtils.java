package com.agero.nccsdk.internal.common.util;

import android.content.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * Created by james hermida on 10/11/17.
 */

public class FileUtils {

    public static boolean isNullOrDoesNotExist(File file) {
        return file == null || !file.exists();
    }

    public static boolean isNull(File file) {
        return file == null;
    }

    public static boolean isNullOrEmpty(File[] files) {
        return files == null || files.length == 0;
    }

    public static boolean isEmpty(File[] files) {
        return files != null && files.length == 0;
    }

    public static void writeAssetToFile(Context context, String assetName, File outFile) {
        try {
            InputStream is = context.getAssets().open(assetName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            FileOutputStream fos = new FileOutputStream(outFile);
            fos.write(buffer);
            fos.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
