package com.agero.nccsdk.internal.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Singleton;

/**
 * Created by james hermida on 12/7/17.
 *
 * Wrapper for System.class to allow for mocking current time
 *
 */

@Singleton
public class NccClock implements Clock {

    private SimpleDateFormat SDF_READABLE_DATE = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ", Locale.US);

    @Override
    public long currentTimeMillis() {
        return System.currentTimeMillis();
    }

    @Override
    public String getLocalTimestamp(long timeMillis) {
        return SDF_READABLE_DATE.format(new Date(timeMillis));
    }
}
