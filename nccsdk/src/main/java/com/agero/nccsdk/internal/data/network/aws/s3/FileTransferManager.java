package com.agero.nccsdk.internal.data.network.aws.s3;

import java.io.File;

public interface FileTransferManager {

    void uploadFile(String targetDestination, File file);

}
