package com.agero.nccsdk.internal.data.preferences;

import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import com.agero.nccsdk.adt.detection.start.model.WifiActivity;
import com.agero.nccsdk.internal.notification.model.NccNotification;
import com.agero.nccsdk.internal.common.util.StringUtils;
import com.google.gson.Gson;

import javax.inject.Singleton;

/**
 * Created by james hermida on 8/14/17.
 */

@Singleton
public class NccSharedPrefs implements SharedPrefs {
    private final SharedPreferences sharedPreferences;
    private final Gson gson;

    private final String KEY_USER_ID = "NCC_KEY_USER_ID";
    private final String KEY_RECENT_WIFI_ACTIVITY = "NCC_KEY_RECENT_WIFI_ACTIVITY";
    private final String KEY_AUTOSTART_LBT = "NCC_KEY_AUTOSTART_LBT";
    private final String KEY_AUTHENTICATED_API_KEY = "NCC_KEY_AUTHENTICATED_API_KEY";
    private final String KEY_ACCESS_ID = "NCC_KEY_AID";

    private final String KEY_NOTIFICATION_PREFIX = "NCC_KEY_NOTIFICATION_";

    public NccSharedPrefs(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
        this.gson = new Gson();
    }

    @Override
    public void setUserId(String userId) {
        put(KEY_USER_ID, userId);
    }

    @Override
    public String getUserId() {
        return getString(KEY_USER_ID);
    }

    @Override
    public void setRecentWifiActivity(WifiActivity lastWifiConnectionInfo) {
        String wifiActivityJson = gson.toJson(lastWifiConnectionInfo);
        getEditor().putString(KEY_RECENT_WIFI_ACTIVITY, wifiActivityJson).commit();
    }

    @Override
    public WifiActivity getRecentWifiActivity() {
        String info = getString(KEY_RECENT_WIFI_ACTIVITY);
        if (StringUtils.isNullOrEmpty(info)) {
            return new WifiActivity();
        } else {
            return gson.fromJson(info, WifiActivity.class);
        }
    }

    @Override
    public void saveNotification(NccNotification notification) {
        String notificationJson = gson.toJson(notification);
        getEditor().putString(KEY_NOTIFICATION_PREFIX + notification.getId(), notificationJson).commit();
    }

    @Override
    public NccNotification getNotification(String id) {
        String notification = getString(KEY_NOTIFICATION_PREFIX + id);
        if (StringUtils.isNullOrEmpty(notification)) {
            return null;
        } else {
            return gson.fromJson(notification, NccNotification.class);
        }
    }

    @Override
    public void setLbtAutoRestart(boolean bool) {
        if (shouldLbtAutoRestart() != bool) {
            put(KEY_AUTOSTART_LBT, bool);
        }
    }

    @Override
    public boolean shouldLbtAutoRestart() {
        return getBoolean(KEY_AUTOSTART_LBT);
    }

    @Override
    public void setAuthenticatedApiKey(String apiKey) {
        put(KEY_AUTHENTICATED_API_KEY, apiKey);
    }

    @Override
    public String getAuthenticatedApiKey() {
        return getString(KEY_AUTHENTICATED_API_KEY);
    }

    @Override
    public void setAccessId(String accessId) {
        put(KEY_ACCESS_ID, accessId);
    }

    @Override
    public String getAccessId() {
        return getString(KEY_ACCESS_ID);
    }

    private SharedPreferences.Editor getEditor() {
        return sharedPreferences.edit();
    }

    // FIXME all methods below should be private and higher-level methods should be exposed for other classes to call, e.g., setSomething() and getSomething()

    public void remove(String key) {
        getEditor().remove(key).commit();
    }

    public String getString(String key) {
        return sharedPreferences.getString(key, null);
    }

    public void put(String key, @Nullable String value) {
        getEditor().putString(key, value).commit();
    }

    public boolean getBoolean(String key) {
        return sharedPreferences.getBoolean(key, false);
    }

    public void put(String key, boolean value) {
        getEditor().putBoolean(key, value).commit();
    }

    public long getLong(String key) {
        return sharedPreferences.getLong(key, 0);
    }

    public long getLong(String key, long defValue) {
        return sharedPreferences.getLong(key, defValue);
    }

    public void put(String key, long value) {
        getEditor().putLong(key, value).commit();
    }

    public float getFloat(String key) {
        return sharedPreferences.getFloat(key, 0);
    }

    public float getFloat(String key, float defValue) {
        return sharedPreferences.getFloat(key, defValue);
    }

    public void put(String key, float value) {
        getEditor().putFloat(key, value).commit();
    }

    public int getInt(String key) {
        return sharedPreferences.getInt(key, 0);
    }

    public int getInt(String key, int defValue) {
        return sharedPreferences.getInt(key, defValue);
    }

    public void put(String key, int value) {
        getEditor().putInt(key, value).commit();
    }
}
