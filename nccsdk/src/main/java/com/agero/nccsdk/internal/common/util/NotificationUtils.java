package com.agero.nccsdk.internal.common.util;

import android.content.Context;
import android.support.v4.app.NotificationCompat;

import com.agero.nccsdk.NccSdk;
import com.agero.nccsdk.R;

import com.agero.nccsdk.internal.log.Timber;

/**
 * Created by james hermida on 8/16/17.
 */

public class NotificationUtils {

    public static void sendNotification(int id, String contentTitle) {
        android.app.NotificationManager mNotificationManager = (android.app.NotificationManager)
                NccSdk.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(NccSdk.getApplicationContext())
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(contentTitle)
                        .setAutoCancel(true);

        mNotificationManager.notify(NotificationUtils.class.getSimpleName(), id, mBuilder.build());
        Timber.v("'%s' notification sent", contentTitle);
    }

}
