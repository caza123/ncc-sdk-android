package com.agero.nccsdk.ubi.collection.io;

import com.agero.nccsdk.internal.log.Timber;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.agero.nccsdk.ubi.collection.io.Headers.HeaderKeyValue.DEFAULT_VALUE_KIND;
import static com.agero.nccsdk.ubi.collection.io.Headers.HeaderKeyValue.DEFAULT_VALUE_VERSION;
import static com.agero.nccsdk.ubi.collection.io.Headers.HeaderKeyValue.KEY_KIND;
import static com.agero.nccsdk.ubi.collection.io.Headers.HeaderKeyValue.KEY_USER_ID;
import static com.agero.nccsdk.ubi.collection.io.Headers.HeaderKeyValue.KEY_VERSION;

public class Headers {

    static final String HEADER_ROW_PREFIX = "# ";
    static final String HEADER_KEY_VALUE_EQUALS_CHAR = ": ";
    static final String HEADER_KEY_VALUE_DELIMITER = ", ";

    private final List<List<HeaderKeyValue>> rows = new ArrayList<>();

    private void addKeyValues(HeaderKeyValue... keyValues) {
        if (keyValues == null || keyValues.length == 0) {
            Timber.w("keyValues is null or empty");
        } else {
            List<HeaderKeyValue> lkv = new ArrayList<>(Arrays.asList(keyValues));
            rows.add(lkv);
        }
    }

    public void addDefaultHeaders(String userId) {
        addKeyValues(
                new HeaderKeyValue(KEY_VERSION, DEFAULT_VALUE_VERSION),
                new HeaderKeyValue(KEY_KIND, DEFAULT_VALUE_KIND)
        );
        addKeyValues(
                new HeaderKeyValue(KEY_USER_ID, userId)
        );
    }

    List<List<HeaderKeyValue>> getRows() {
        return rows;
    }

    static class HeaderKeyValue extends AbstractMap.SimpleEntry<String, String> {

        static final String KEY_VERSION = "Version"; static final String DEFAULT_VALUE_VERSION = "1.2.1";
        static final String KEY_KIND = "Kind";       static final String DEFAULT_VALUE_KIND = "Sensor";

        static final String KEY_USER_ID = "UserId";

        HeaderKeyValue(String key, String value) {
            super(key, value);
        }
    }
}
