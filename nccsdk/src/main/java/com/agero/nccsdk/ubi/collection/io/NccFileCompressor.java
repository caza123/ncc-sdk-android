package com.agero.nccsdk.ubi.collection.io;

import com.agero.nccsdk.internal.common.util.FileUtils;
import com.agero.nccsdk.internal.common.util.StringUtils;

import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.utils.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.InvalidParameterException;
import java.util.zip.GZIPOutputStream;

import com.agero.nccsdk.internal.log.Timber;
import com.agero.nccsdk.internal.common.io.FileCompressor;

/**
 * Created by james hermida on 10/30/17.
 */

public class NccFileCompressor implements FileCompressor {

    @Override
    public File gzip(File source, String outputDir) {
        if (FileUtils.isNullOrDoesNotExist(source)) {
            throw new InvalidParameterException("source is null or empty");
        } else if (StringUtils.isNullOrEmpty(outputDir)) {
            throw new InvalidParameterException("outputDir is null or empty");
        } else {
            // Create output file if needed
            File outputDirFileHandler = new File(outputDir);
            if (!outputDirFileHandler.exists()) {
                outputDirFileHandler.mkdir();
            }

            // Zip data
            String outputAbsPath = outputDir + File.separator + source.getName();
            if (!outputAbsPath.endsWith(EXTENSION_GZIP)) {
                outputAbsPath += EXTENSION_GZIP;
            }
            File outputFile = new File(outputAbsPath);
            if (outputFile.exists()) {
                Timber.w("Not gzipping file since it already exists: %s", outputAbsPath);
                return null;
            } else {
                zip(source.getAbsolutePath(), outputAbsPath);
                return outputFile;
            }
        }
    }

    private void zip(String input, String zipFileName) {
        byte[] buffer = new byte[1024];
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(zipFileName);
            GZIPOutputStream gzipOuputStream = new GZIPOutputStream(fileOutputStream);

            FileInputStream fi = new FileInputStream(input);
            int bytes_read;

            while ((bytes_read = fi.read(buffer)) > 0) {
                gzipOuputStream.write(buffer, 0, bytes_read);
            }

            fi.close();
            gzipOuputStream.finish();
            gzipOuputStream.close();
        } catch (Exception e) {
            Timber.e(e, "Exception zipping input %s to %s", input, zipFileName);
        }
    }

    @Override
    public File tar(File inDirectory, String outputTarFileAbsolutePath) throws IOException, ArchiveException {
        if (StringUtils.isNullOrEmpty(outputTarFileAbsolutePath)) {
            throw new InvalidParameterException("outputTarFileAbsolutePath is null or empty");
        } else if (inDirectory == null || inDirectory.listFiles().length == 0) {
            throw new InvalidParameterException("inDirectory is null or empty");
        } else {
            if (!outputTarFileAbsolutePath.endsWith(EXTENSION_TAR)) {
                outputTarFileAbsolutePath += EXTENSION_TAR;
            }
            File out = new File(outputTarFileAbsolutePath);
            Timber.v("Creating tar archive: %s", out.getAbsolutePath());

            // Output stream that will hold the physical TAR file
            OutputStream tarFileOutputStream = new FileOutputStream(out);

            // Create ArchiveOutputStream that attaches file output stream and specifies TAR as type of compression
            ArchiveOutputStream tarArchiveOutputStream = new ArchiveStreamFactory().createArchiveOutputStream(ArchiveStreamFactory.TAR, tarFileOutputStream);

            // Iterate through gzip files and add to archive
            File[] files = inDirectory.listFiles();
            if (files != null && files.length > 0) {
                int filesCount = files.length;
                for (int i = 0; i < filesCount; i++) {
                    // Add file to archive
                    tarArchiveOutputStream = addFileToArchive(tarArchiveOutputStream, files[i]);
                }
            }

            // Finish the addition of adding entries to the archive
            tarArchiveOutputStream.finish();

            // Close output stream, our files are zipped
            tarFileOutputStream.close();

            return out;
        }
    }

    /**
     * Adds an archive entry created from the fileToArchive to the tarArchiveOutputStream.
     *
     * @param tarArchiveOutputStream The {@link ArchiveOutputStream} the entry will be added to
     * @param fileToArchive          The {@link File} the archive entry is created from
     * @return The {@link ArchiveOutputStream} the archive entry was added to
     * @throws IOException
     */
    private ArchiveOutputStream addFileToArchive(ArchiveOutputStream tarArchiveOutputStream, File fileToArchive) throws IOException {
        if (tarArchiveOutputStream == null) {
            throw new InvalidParameterException("tarArchiveOutputStream is null");
        } else if (fileToArchive == null) {
            throw new InvalidParameterException("fileToArchive is null");
        } else {
            Timber.v("Adding file to tar archive: %s", fileToArchive);

            // Create TarArchiveEntry - write header information
            TarArchiveEntry tarArchiveEntry = new TarArchiveEntry(fileToArchive.getName());

            // Length of the TAR file needs to be set using setSize method
            tarArchiveEntry.setSize(fileToArchive.length());
            tarArchiveOutputStream.putArchiveEntry(tarArchiveEntry);
            IOUtils.copy(new FileInputStream(fileToArchive), tarArchiveOutputStream);

            // Close archive entry, write trailer information
            tarArchiveOutputStream.closeArchiveEntry();

            return tarArchiveOutputStream;
        }
    }
}
