package com.agero.nccsdk.adt.detection.start.algo;

import android.content.Context;

import com.agero.nccsdk.domain.data.NccMotionData;
import com.agero.nccsdk.adt.AdtTriggerType;
import com.agero.nccsdk.internal.log.Timber;
import com.google.android.gms.location.DetectedActivity;

/**
 * Created by james hermida on 11/30/17.
 */

public class MotionDrivingStartStrategy extends DrivingStartStrategy<NccMotionData> {

    private final int THRESHOLD_MOTION_CONFIDENCE;

    public MotionDrivingStartStrategy(Context context, int threshold) {
        super(context);
        THRESHOLD_MOTION_CONFIDENCE = threshold;
    }

    @Override
    public void evaluate(NccMotionData data) {
        if (data == null) {
            Timber.e("Unable to evaluate motion for start strategy. data is null");
        } else if (data.getActivityType() == DetectedActivity.IN_VEHICLE
                && data.getConfidence() >= THRESHOLD_MOTION_CONFIDENCE) {
            sendCollectionStartBroadcast(AdtTriggerType.START_MOTION);
        }
    }

    @Override
    public void releaseResources() {

    }
}
