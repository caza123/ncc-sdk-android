package com.agero.nccsdk.domain.data;

public class NccPhoneCallsData extends NccAbstractSensorData {

    public enum State {
        INCOMING_CALL_RECEIVED("Incoming Call Received"),
        INCOMING_CALL_ANSWERED("Incoming Call Answered"),
        INCOMING_CALL_ENDED("Incoming Call Ended"),
        OUTGOING_CALL_STARTED("Outgoing Call Started"),
        OUTGOING_CALL_ENDED("Outgoing Call Ended"),
        MISSED_CALL("Missed Call");

        String name;

        State(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "State{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }

    private final State state;

    public NccPhoneCallsData(long timestamp, State state) {
        super(timestamp);
        this.state = state;
    }

    public State getState() {
        return state;
    }

    @Override
    public String toString() {
        return "NccPhoneCallsData{" +
                "state=" + state.toString() +
                ", timestamp=" + timestamp +
                '}';
    }
}
