package com.agero.nccsdk.domain.data;

public class NccPowerData extends NccAbstractSensorData {

    private boolean isConnected;

    public NccPowerData(long timestamp, boolean isConnected) {
        super(timestamp);
        this.isConnected = isConnected;
    }

    public boolean isConnected() {
        return isConnected;
    }

    @Override
    public String toString() {
        return "NccPowerData{" +
                "isConnected=" + isConnected +
                ", timestamp=" + timestamp +
                '}';
    }
}
