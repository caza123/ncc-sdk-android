package com.agero.nccsdk.domain.data;

import org.json.JSONException;

/**
 * Created by james hermida on 8/18/17.
 *
 * Abstract class to serve as a base class for all sensor data models.
 */

public abstract class NccAbstractSensorData implements NccSensorData {

    final long timestamp;

    /**
     * Creates an instance of NccAbstractSensorData. Invoked by a subclass's constructor.
     *
     * @param timestamp UTC timestamp in milliseconds
     */
    NccAbstractSensorData(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Gets the timestamp of this sensor reading
     *
     * @return long representing the UTC timestamp
     */
    @SuppressWarnings("unused")
    public long getTimestamp() {
        return timestamp;
    }
}
