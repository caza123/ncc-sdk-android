package com.agero.nccsdk.domain.model;

public class NccBluetoothDevice {

    private String name;
    private String address;
    private boolean isConnected;
    private NccBluetoothClass bluetoothClass;

    public NccBluetoothDevice(String name, String address, boolean isConnected, NccBluetoothClass bluetoothClass) {
        this.name = name;
        this.address = address;
        this.isConnected = isConnected;
        this.bluetoothClass = bluetoothClass;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public NccBluetoothClass getBluetoothClass() {
        return bluetoothClass;
    }

    @Override
    public String toString() {
        return "NccBluetoothDevice{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", isConnected=" + isConnected +
                ", bluetoothClass=" + bluetoothClass.toString() +
                '}';
    }
}
