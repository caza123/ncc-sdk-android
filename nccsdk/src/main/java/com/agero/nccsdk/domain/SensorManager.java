package com.agero.nccsdk.domain;

import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccSensorListener;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.config.NccConfig;

/**
 * Created by james hermida on 10/24/17.
 */

public interface SensorManager {

    void subscribeSensorListener(NccSensorType sensorType, NccSensorListener sensorListener, NccConfig config) throws NccException;

    void unsubscribeSensorListener(NccSensorType sensorType, NccSensorListener sensorListener) throws NccException;

    void setConfig(NccSensorType sensorType, NccConfig config) throws NccException;

    NccConfig getConfig(NccSensorType sensorType) throws NccException;

    boolean isSensorAvailable(NccSensorType sensorType) throws NccException;
}
