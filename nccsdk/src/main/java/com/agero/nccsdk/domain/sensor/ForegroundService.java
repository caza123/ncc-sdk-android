package com.agero.nccsdk.domain.sensor;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.agero.nccsdk.NccSdk;
import com.agero.nccsdk.R;
import com.agero.nccsdk.internal.data.DataManager;
import com.agero.nccsdk.internal.log.Timber;
import com.agero.nccsdk.internal.notification.model.NccNotification;

import javax.inject.Inject;

import static android.app.NotificationManager.IMPORTANCE_HIGH;

/**
 * Created by james hermida on 8/28/17.
 */

public class ForegroundService extends Service {

    public static final int NOTIFICATION_ID = 962011;

    private final int DEFAULT_NOTIFICATION_DRAWABLE = R.drawable.ic_launcher;
    private final String DEFAULT_NOTIFICATION_CONTENT_TITLE = "Monitoring Location";
    private final String DEFAULT_NOTIFICATION_CONTENT_TEXT = "";

    @Inject
    DataManager dataManager;

    @Override
    public void onCreate() {
        super.onCreate();
        NccSdk.getComponent().inject(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startForeground();
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopAll();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Timber.w("App killed by user while collecting location data");
        super.onTaskRemoved(rootIntent);
        stopAll();
    }

    private void stopAll() {
        stopForeground(true);
        stopSelf();
    }

    /**
     * Required to support Android O limitations with getting locations in the background
     */
    private void startForeground() {
        // Notification channel needs to be created for Android O
        String CHANNEL_ID = String.valueOf(NOTIFICATION_ID);
        String CHANNEL_NAME = getPackageName() + "." + ForegroundService.class.getSimpleName();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME, IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setShowBadge(true);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (manager == null) {
                Timber.e("Unable to create notification channel. NotificationManager is null.");
            } else {
                manager.createNotificationChannel(notificationChannel);
            }
        }

        // Set up notification to be displayed
        NotificationCompat.Builder builder;
        NccNotification notification = dataManager.getNotification(String.valueOf(NOTIFICATION_ID));
        if (notification == null) {
            builder = new NotificationCompat.Builder(this)
                    .setChannelId(CHANNEL_ID)
                    .setSmallIcon(DEFAULT_NOTIFICATION_DRAWABLE)
                    .setContentTitle(DEFAULT_NOTIFICATION_CONTENT_TITLE)
                    .setContentText(DEFAULT_NOTIFICATION_CONTENT_TEXT);
        } else {
            builder = new NotificationCompat.Builder(this)
                    .setChannelId(CHANNEL_ID)
                    .setSmallIcon(notification.getDrawableId())
                    .setContentTitle(notification.getContentTitle())
                    .setContentText(notification.getContentText());
        }

        startForeground(NOTIFICATION_ID, builder.build());
    }

}
