package com.agero.nccsdk.domain.data;

import java.util.Locale;

/**
 * Created by james hermida on 8/24/17.
 */

public class NccRotationVectorData extends NccAbstractSensorData {

    private final float quaternionW;
    private final float quaternionX;
    private final float quaternionY;
    private final float quaternionZ;

    /**
     * Creates an instance of NccRotationVectorData
     * @param timestamp UTC timestamp in milliseconds
     * @param quaternionW Real part of the device attitude quaternion where z is in the vertical direction and y is arbitrary
     * @param quaternionX i imaginary part of the device attitude quaternion where z is in the vertical direction and y is arbitrary
     * @param quaternionY j imaginary part of the device attitude quaternion where z is in the vertical direction and y is arbitrary
     * @param quaternionZ k imaginary part of the device attitude quaternion where z is in the vertical direction and y is arbitrary
     */
    public NccRotationVectorData(long timestamp, float quaternionW, float quaternionX, float quaternionY, float quaternionZ) {
        super(timestamp);
        this.quaternionW = quaternionW;
        this.quaternionX = quaternionX;
        this.quaternionY = quaternionY;
        this.quaternionZ = quaternionZ;
    }

    /**
     * Gets the real part of the device attitude quaternion where z is in the vertical direction and y is arbitrary
     *
     * @return float of the device attitude quaternion
     */
    public float getQuaternionW() {
        return quaternionW;
    }

    /**
     * Gets the i imaginary part of the device attitude quaternion where z is in the vertical direction and y is arbitrary
     *
     * @return float of the i imaginary part of the device attitude quaternion
     */
    public float getQuaternionX() {
        return quaternionX;
    }

    /**
     * Gets the j imaginary part of the device attitude quaternion where z is in the vertical direction and y is arbitrary
     *
     * @return float of the j imaginary part of the device attitude quaternion
     */
    public float getQuaternionY() {
        return quaternionY;
    }

    /**
     * Gets the z imaginary part of the device attitude quaternion where z is in the vertical direction and y is arbitrary
     *
     * @return float of the z imaginary part of the device attitude quaternion
     */
    public float getQuaternionZ() {
        return quaternionZ;
    }

    @Override
    public String toString() {
        return "NccRotationVectorData{" +
                "quaternionW=" + quaternionW +
                ", quaternionX=" + quaternionX +
                ", quaternionY=" + quaternionY +
                ", quaternionZ=" + quaternionZ +
                ", timestamp=" + timestamp +
                '}';
    }
}
