package com.agero.nccsdk.domain.sensor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v4.content.LocalBroadcastManager;

import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.config.NccConfig;
import com.agero.nccsdk.internal.log.Timber;

/**
 * Abstract class for sensors whose data is received from the system via {@link BroadcastReceiver}.
 * This class receives the broadcast on the main thread, then pushes the result to a background
 * thread to be processed. The subclass provides an implementation via {@link NccBroadcastReceiverDelegate}
 * on how the broadcast will be processed.
 *
 * This class also abstracts registering for broadcasts with the system via {@link #getActions()}
 * and {@link #isLocalReceiver()}.
 */
abstract class NccAbstractBroadcastReceiverSensor extends NccAbstractSensor implements NccBroadcastReceiverDelegate {

    private final String TAG;

    private BroadcastReceiver broadcastReceiver;
    private Handler handler;
    private HandlerThread handlerThread;

    NccAbstractBroadcastReceiverSensor(Context context, String tag, NccSensorType sensorType, NccConfig config) {
        super(context, sensorType, config);
        TAG = tag;
    }

    @Override
    public void startStreaming() {
        String[] actions = getActions();
        if (actions == null || actions.length == 0) {
            Timber.e("Cannot start sensor. Actions is null.");
        } else {
            startHandlers();
            broadcastReceiver = new NccBroadcastReceiver(handler, this);
            registerReceiver(broadcastReceiver, getIntentFilter(actions));
            isStreaming = true;
        }
    }

    @Override
    public void stopStreaming() {
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
            stopHandlers();
        }
        isStreaming = false;
    }

    private void startHandlers() {
        handlerThread = new HandlerThread(TAG + HandlerThread.class.getSimpleName());
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
    }
    
    private void stopHandlers() {
        if (handlerThread != null && handlerThread.isAlive()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                Timber.v("%s sensor event handler thread quit safely result: %s", sensorType.getName(), handlerThread.quitSafely());
            } else {
                Timber.v("%s sensor event handler thread quit result: %s", sensorType.getName(), handlerThread.quit());
            }
        }
    }

    private IntentFilter getIntentFilter(String[] actions) {
        IntentFilter intentFilter = new IntentFilter();
        if (actions != null && actions.length > 0) {
            for (int i = 0; i < actions.length; i++) {
                intentFilter.addAction(actions[i]);
            }
        }

        return intentFilter;
    }

    private void registerReceiver(BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        if (isLocalReceiver()) {
            LocalBroadcastManager.getInstance(applicationContext).registerReceiver(broadcastReceiver, intentFilter);
        } else {
            applicationContext.registerReceiver(broadcastReceiver, intentFilter);
        }
    }

    private void unregisterReceiver(BroadcastReceiver broadcastReceiver) {
        if (isLocalReceiver()) {
            LocalBroadcastManager.getInstance(applicationContext).unregisterReceiver(broadcastReceiver);
        } else {
            applicationContext.unregisterReceiver(broadcastReceiver);
        }
    }

    /**
     * Get the list of actions required for registering the {@link BroadcastReceiver}
     */
    abstract String[] getActions();

    /**
     * Determines if the {@link BroadcastReceiver} should be registered for local or global broadcasts
     */
    abstract boolean isLocalReceiver();
}
