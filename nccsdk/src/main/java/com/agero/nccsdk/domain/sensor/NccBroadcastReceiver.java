package com.agero.nccsdk.domain.sensor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

/**
 * Class created to delegate broadcasts received on the main thread to the provided handler
 */
public class NccBroadcastReceiver extends BroadcastReceiver {

    private Handler handler;
    private NccBroadcastReceiverDelegate nccBroadcastReceiverDelegate;

    public NccBroadcastReceiver(Handler handler, NccBroadcastReceiverDelegate nccBroadcastReceiverDelegate) {
        super();
        this.handler = handler;
        this.nccBroadcastReceiverDelegate = nccBroadcastReceiverDelegate;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        handler.post(new NccRunnable(context, intent, nccBroadcastReceiverDelegate));
    }
}
