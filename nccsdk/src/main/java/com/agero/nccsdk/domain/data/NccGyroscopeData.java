package com.agero.nccsdk.domain.data;

import java.util.Locale;

/**
 * Created by james hermida on 8/29/17.
 */

public class NccGyroscopeData extends NccAbstractSensorData {

    private final float rotationX;
    private final float rotationY;
    private final float rotationZ;

    /**
     * Creates an instance of NccGyroscopeData
     *
     * @param timestamp UTC timestamp in milliseconds
     * @param rotationX The X-axis rotation rate in radians per second
     * @param rotationY The Y-axis rotation rate in radians per second
     * @param rotationZ The Z-axis rotation rate in radians per second
     */
    public NccGyroscopeData(long timestamp, float rotationX, float rotationY, float rotationZ) {
        super(timestamp);
        this.rotationX = rotationX;
        this.rotationY = rotationY;
        this.rotationZ = rotationZ;
    }

    /**
     * Gets the X-axis rotation rate in radians per second. The sign follows the right hand rule:
     * If the right hand is wrapped around the X axis such that the tip of the thumb points toward positive X,
     * a positive rotation is one toward the tips of the other four fingers.
     *
     * @return float of the X-axis rotation rate in radians per second
     */
    public float getRotationX() {
        return rotationX;
    }

    /**
     * Gets the Y-axis rotation rate in radians per second. The sign follows the right hand rule:
     * If the right hand is wrapped around the Y axis such that the tip of the thumb points toward positive Y,
     * a positive rotation is one toward the tips of the other four fingers.
     *
     * @return float of the Y-axis rotation rate in radians per second
     */
    public float getRotationY() {
        return rotationY;
    }

    /**
     * Gets the Z-axis rotation rate in radians per second. The sign follows the right hand rule:
     * If the right hand is wrapped around the Z axis such that the tip of the thumb points toward positive Z,
     * a positive rotation is one toward the tips of the other four fingers.
     *
     * @return float of the Z-axis rotation rate in radians per second
     */
    public float getRotationZ() {
        return rotationZ;
    }

    @Override
    public String toString() {
        return "NccGyroscopeData{" +
                "rotationX=" + rotationX +
                ", rotationY=" + rotationY +
                ", rotationZ=" + rotationZ +
                ", timestamp=" + timestamp +
                '}';
    }
}
