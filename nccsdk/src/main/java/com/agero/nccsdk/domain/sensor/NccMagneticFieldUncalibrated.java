package com.agero.nccsdk.domain.sensor;

import android.content.Context;
import android.hardware.SensorEvent;

import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.config.NccConfig;
import com.agero.nccsdk.domain.data.NccAbstractSensorData;
import com.agero.nccsdk.domain.data.NccMagneticFieldUncalibratedData;

/**
 * Created by james hermida on 8/24/17.
 */

public class NccMagneticFieldUncalibrated extends NccAbstractNativeSensor {

    /**
     * Creates an instance of NccMagneticFieldUncalibrated
     *
     * @param context The Context allowing access to application-specific resources and classes, as well as
     * up-calls for application-level operations such as launching activities,
     * broadcasting and receiving intents, etc.
     * @throws NccException Exception if there is an error constructing the instance
     */
    public NccMagneticFieldUncalibrated(Context context, NccConfig config) throws NccException {
        super(context, NccMagneticFieldUncalibrated.class.getSimpleName(), NccSensorType.MAGNETIC_FIELD_UNCALIBRATED, config);
    }

    /**
     * See {@link NccAbstractNativeSensor#buildData(SensorEvent, long)}
     */
    @Override
    protected NccAbstractSensorData buildData(SensorEvent sensorEvent, long sensorEventTime) {
        return new NccMagneticFieldUncalibratedData(sensorEventTime,
            sensorEvent.values[0],
            sensorEvent.values[1],
            sensorEvent.values[2],
            sensorEvent.values[3],
            sensorEvent.values[4],
            sensorEvent.values[5],
            sensorEvent.accuracy);
    }
}
