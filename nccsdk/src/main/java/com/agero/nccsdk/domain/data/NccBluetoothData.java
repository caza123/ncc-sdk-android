package com.agero.nccsdk.domain.data;

import com.agero.nccsdk.domain.model.NccBluetoothDevice;

public class NccBluetoothData extends NccAbstractSensorData {

    private final NccBluetoothDevice device;

    public NccBluetoothData(long timestamp, NccBluetoothDevice device) {
        super(timestamp);
        this.device = device;
    }

    public NccBluetoothDevice getDevice() {
        return device;
    }

    @Override
    public String toString() {
        return "NccBluetoothData{" +
                "device=" + device.toString() +
                ", timestamp=" + timestamp +
                '}';
    }
}
